require "crsfml"
require "./global_config"
require "./game_shapes"
require "../lib/crsfml/src/audio/audio"

module Snake
    class GameGrid
        def initialize(window : SF::RenderWindow)
            @scale = 1.0.to_f32
            @x_added = 0.0.to_f32
            @y_added = 0.0.to_f32
            @global_config = Snake::GlobalConfig.instance
            @grid = {} of {Int32, Int32} => SF::Shape?
            @current_render = SF::RenderTexture.new(@global_config.render_width, @global_config.render_height)
            @width = (@global_config.render_width.as(Int32) / 16).to_i32.as(Int32)
            @height = (@global_config.render_height.as(Int32) / 16).to_i32.as(Int32)
            calculate_scaling!(window.size.x, window.size.y)
            @score_text = SF::Text.new
            @score_content_text = SF::Text.new
            @score_text.font = @global_config.default_font
            @score_content_text.font = @global_config.default_font
            @score_text.character_size = 24
            @score_content_text.character_size = 24
            @score_text.string = "SCORE:"
            @score_text.color = @global_config.text_color
            @score_content_text.color = @global_config.text_color
            @score_text.style = SF::Text::Bold
            @state = "main"
            @levels = [] of String
            Dir["levels/*"].each do |e|
                @levels << File.basename(e, ".slvl")
            end
            @main_menu_active_item = 2
            @main_menu_active_level = 0
            @main_menu_selected_speed = 5
            @main_menu_selected_speed_text = SF::Text.new
            @main_menu_selected_speed_text.font = @global_config.default_font
            @main_menu_selected_speed_text.character_size = 24
            @main_menu_speed_label = SF::Text.new
            @main_menu_speed_label.font = @global_config.default_font
            @main_menu_speed_label.character_size = 18
            @main_menu_speed_label.string = "SPEED"
            @main_menu_speed_hint = SF::Text.new
            @main_menu_speed_hint.font = @global_config.default_font
            @main_menu_speed_hint.character_size = 14
            @main_menu_speed_hint.string = "(moves per second)"
            @main_menu_speed_hint.color = @global_config.text_color
            @main_menu_selected_level_text = SF::Text.new
            @main_menu_selected_level_text.font = @global_config.default_font
            @main_menu_selected_level_text.character_size = 24
            @main_menu_level_label = SF::Text.new
            @main_menu_level_label.font = @global_config.default_font
            @main_menu_level_label.character_size = 18
            @main_menu_level_label.string = "LEVEL"
            @main_menu_start_text = SF::Text.new
            @main_menu_start_text.font = @global_config.default_font
            @main_menu_start_text.character_size = 24
            @main_menu_start_text.string = "START"
            @main_menu_quit_text = SF::Text.new
            @main_menu_quit_text.font = @global_config.default_font
            @main_menu_quit_text.character_size = 24
            @main_menu_quit_text.string = "QUIT"
            @snake_clock = SF::Clock.new
            @next_snake_dir = nil.as(Char | Nil)
            @snake = nil.as(Array(SF::CircleShape) | Nil)
            @pause_menu_items = ["CONTINUE", "RESTART", "MAIN MENU", "QUIT"]
            @pause_menu_active_item = 0
            @death_menu_items = ["RESTART", "MAIN MENU", "QUIT"]
            @death_menu_active_item = 0
            @window = window
@spawn_point = {(@width/2).round.to_i32, (@height/2+1).round.to_i32}
            @score = 0
            clear
            @food_point = nil.as(Tuple(Int32, Int32) | Nil)
            @bonus_point = nil.as(Tuple(Int32, Int32) | Nil)
            @bonus_moves = 0
            @sounds = true
            @speaker_texture = SF::Texture.from_file("images/speaker_texture.png")
            @speaker_texture.smooth = true
            @sounds_enabled_sprite = SF::Sprite.new(@speaker_texture)
            @sounds_enabled_sprite.texture_rect = SF.int_rect(0, 0, 32, 32)
            @sounds_disabled_sprite = SF::Sprite.new(@speaker_texture)
            @sounds_disabled_sprite.texture_rect = SF.int_rect(32, 0, 32, 32)
            @sound_buffer_click = SF::SoundBuffer.from_file("sounds/click.ogg")
            @sound_buffer_blip = SF::SoundBuffer.from_file("sounds/blip.ogg")
            @sound_buffer_airhorn = SF::SoundBuffer.from_file("sounds/airhorn.ogg")
            @sound_buffer_ded = SF::SoundBuffer.from_file("sounds/ded.ogg")
            @current_sound = SF::Sound.new
        end

        def clear
            0.upto(@width-1) do |i|
                2.upto(@height-1) do |j|
                    @grid[{i,j}] = nil
                end
            end
        end

        def play_sound(handle)
            if @sounds
                @current_sound.stop
                case handle
                when "click"
                    @current_sound.buffer = @sound_buffer_click
                when "blip"
                    @current_sound.buffer = @sound_buffer_blip
                when "airhorn"
                    @current_sound.buffer = @sound_buffer_airhorn
                when "ded"
                    @current_sound.buffer = @sound_buffer_ded
                end
                @current_sound.play
            end
        end

        def load_level(path)
            current_level = [] of Int8
            File.open(path) do |f|
                f.each_char do |c|
                    if c != '\r' && c != '\n' && c != ' '
                        current_level << c.to_i8
                    end
                end
            end
            clear
            2.upto(@height-1) do |i|
                0.upto(@width-1) do |j|
                    case current_level[((i-2)*@width)+j]
                    when 1
                        @grid[{j,i}] = Snake::GameShapes.wall_square({j,i})
                    when 2
                        @spawn_point = {j,i}.as(Tuple(Int32,Int32))
                    end
                end
            end
        end

        def spawn_snake(speed)
            @snake = [Snake::GameShapes.snake_part({@spawn_point[0]-2, @spawn_point[1]}), Snake::GameShapes.snake_part({@spawn_point[0]-1, @spawn_point[1]}), Snake::GameShapes.snake_part({@spawn_point[0], @spawn_point[1]})]
            @snake_dir = 'r'
            @next_snake_dir = nil
            @snake_speed = speed.as(Int32)
            @snake_clock.restart
        end

        def change_snake_dir(dir)
            case @snake_dir
            when 'r'
                @snake_dir = dir.as(Char) if dir != 'l'
            when 'l'
                @snake_dir = dir.as(Char) if dir != 'r'
            when 'u'
                @snake_dir = dir.as(Char) if dir != 'd'
            when 'd'
                @snake_dir = dir.as(Char) if dir != 'u'
            end
        end

        def spawn_food
            x = 0
            y = 0
            ok = true
            0.upto(100) do
                x = (0...@width).to_a.sample
                y = (2...@height).to_a.sample
                ok = @grid[{x, y}].nil?
                if ok
                    @snake.as(Array(SF::CircleShape)).each do |sp|
                        ok = ((sp.position.x/16).to_i32 != x && (sp.position.y/16).to_i32 != y)
                        break unless ok
                    end
                end
                break if ok
            end
            if ok
                @grid[{x, y}] = Snake::GameShapes.food_square({x, y})
                @food_point = {x, y}
            else
                raise Exception.new("Could not spawn next food in 100 tries!")
            end
        end

        def despawn_food
            unless @food_point.nil?
                @grid[@food_point.as(Tuple(Int32, Int32))] = nil
                @food_point = nil
            end
        end

        def spawn_bonus
            if @bonus_moves <= -30
                if rand(10) == 7
                    x = 0
                    y = 0
                    min_x = ((@snake.as(Array(SF::CircleShape)).last.position.x.to_i32/16)-18).to_i32
                    if min_x < 0
                        min_x = 0
                    end
                    max_x = ((@snake.as(Array(SF::CircleShape)).last.position.x.to_i32/16)+18).to_i32
                    if max_x >= @width
                        max_x = @width-1
                    end
                    min_y = ((@snake.as(Array(SF::CircleShape)).last.position.y.to_i32/16)-18).to_i32
                    if min_y < 2
                        min_y = 2
                    end
                    max_y = ((@snake.as(Array(SF::CircleShape)).last.position.y.to_i32/16)+18).to_i32
                    if max_y >= @height
                        max_y = @height-1
                    end
                    ok = false
                    0.upto(20) do
                        x = (min_x..max_x).to_a.sample
                        y = (min_y..max_y).to_a.sample
                        ok = ((x < (@snake.as(Array(SF::CircleShape)).last.position.x.to_i32/16)-5 || x > (@snake.as(Array(SF::CircleShape)).last.position.x.to_i32/16)+5) && (y < (@snake.as(Array(SF::CircleShape)).last.position.y.to_i32/16)-5 || y > (@snake.as(Array(SF::CircleShape)).last.position.y.to_i32/16)+5))
                        if ok
                            ok = @grid[{x, y}].nil?
                            if ok
                                @snake.as(Array(SF::CircleShape)).each do |sp|
                                    ok = ((sp.position.x/16).to_i32 != x && (sp.position.y/16).to_i32 != y)
                                    break unless ok
                                end
                            end
                        end
                        break if ok
                    end
                    if ok
                        @grid[{x,y}] = Snake::GameShapes.bonus_triangle({x,y})
                        @bonus_point = {x,y}
                        @bonus_moves = 30
                    end
                else
                    @bonus_moves = 0
                end
            end
        end

        def despawn_bonus
            unless @bonus_point.nil?
                @grid[@bonus_point.as(Tuple(Int32, Int32))] = nil
                @bonus_point = nil
                @bonus_moves = 0
            end
        end

        def move_snake
            if !@snake.nil?
                snake = @snake.as(Array(SF::CircleShape))
                if @snake_clock.elapsed_time.as_seconds >= 1.0/@snake_speed.as(Int32)
                    @bonus_moves -= 1
                    if @bonus_moves <= 0
                        despawn_bonus
                    end
                    spawn_bonus
                    change_snake_dir(@next_snake_dir) unless @next_snake_dir.nil?
                    @next_snake_dir = nil
                    case @snake_dir
                    when 'r'
                        next_snake_point = {(snake.last.position.x/16).to_i32+1, (snake.last.position.y/16).to_i32}
                        if next_snake_point[0] >= @width
                            next_snake_point = {0, next_snake_point[1]}
                        end
                    when 'l'
                        next_snake_point = {(snake.last.position.x/16).to_i32-1, (snake.last.position.y/16).to_i32}
                        if next_snake_point[0] < 0
                            next_snake_point = {@width-1, next_snake_point[1]}
                        end
                    when 'u'
                        next_snake_point = {(snake.last.position.x/16).to_i32, (snake.last.position.y/16).to_i32-1}
                        if next_snake_point[1] < 2
                            next_snake_point = {next_snake_point[0], @height-1}
                        end
                    when 'd'
                        next_snake_point = {(snake.last.position.x/16).to_i32, (snake.last.position.y/16).to_i32+1}
                        if next_snake_point[1] >= @height
                            next_snake_point = {next_snake_point[0], 2}
                        end
                    end
                    snake.each do |sp|
                        if sp.position.x.to_i32/16 == next_snake_point.as(Tuple(Int32, Int32))[0] && sp.position.y.to_i32/16 == next_snake_point.as(Tuple(Int32, Int32))[1]
                            die!
                            break
                        end
                    end
                    if @state == "running"
                        next_grid_point = @grid[next_snake_point]
                        if next_grid_point.nil?
                            snake << Snake::GameShapes.snake_part(next_snake_point.as(Tuple(Int32, Int32)))
                            snake.delete_at(0)
                            @snake = snake
                            @snake_clock.restart
                        elsif next_grid_point.is_a?(SF::RectangleShape) && next_grid_point.fill_color == @global_config.wall_color
                            die!
                        elsif next_grid_point.is_a?(SF::RectangleShape) && next_grid_point.fill_color == @global_config.food_color
                            play_sound("blip")
                            snake << Snake::GameShapes.snake_part(next_snake_point.as(Tuple(Int32, Int32)))
                            @grid[next_snake_point.as(Tuple(Int32, Int32))] = nil
                            @score += 10
                            spawn_food
                            @snake_clock.restart
                        elsif next_grid_point.is_a?(SF::ConvexShape) && next_grid_point.fill_color == @global_config.bonus_color
                            play_sound("airhorn")
                            snake << Snake::GameShapes.snake_part(next_snake_point.as(Tuple(Int32, Int32)))
                            snake.delete_at(0)
                            @score += @bonus_moves*50
                            despawn_bonus
                            @snake_clock.restart
                        end
                    end
                end
            end
        end

        def draw_frame
            @current_render.clear(@global_config.background_color)
            unless main?
                @score_text.position = SF.vector2(3,3)
                @score_content_text.string = " #{@score}"
                @score_content_text.position = SF.vector2(3+@score_text.global_bounds.width,3)
                0.upto(@width-1) do |i|
                    2.upto(@height-1) do |j|
                        shape = @grid[{i,j}]
                        if !shape.nil?
                            @current_render.draw(shape)
                        end
                    end
                end
                if !@bonus_point.nil? && @bonus_moves > 0
                    bonus_moves_text = SF::Text.new
                    bonus_moves_text.font = @global_config.default_font
                    bonus_moves_text.character_size = 12
                    bonus_moves_text.string = @bonus_moves.to_s
                    bonus_moves_text.color = @global_config.text_color
                    bonus_moves_text.position = SF.vector2(@bonus_point.as(Tuple(Int32,Int32))[0]*16+8-((bonus_moves_text.local_bounds.width)/2), @bonus_point.as(Tuple(Int32,Int32))[1]*16+8-((bonus_moves_text.local_bounds.height)/2))
                    @current_render.draw(bonus_moves_text)
                end
                if !@snake.nil?
                    @snake.as(Array(SF::CircleShape)).each do |s|
                        @current_render.draw(s)
                    end
                end
                if paused?
                    @current_render.draw(Snake::GameShapes.pause_overlay)
                    @pause_menu = [] of SF::Text
                    game_paused_text = SF::Text.new
                    game_paused_text.font = @global_config.default_font
                    game_paused_text.character_size = 24
                    game_paused_text.string = "G A M E  P A U S E D"
                    game_paused_text.color = @global_config.text_color
                    game_paused_text.style = SF::Text::Bold
                    @pause_menu_items.each do |i|
                        item_text = SF::Text.new
                        item_text.font = @global_config.default_font
                        item_text.character_size = 24
                        item_text.string = i
                        item_text.color = @global_config.text_color
                        @pause_menu.as(Array(SF::Text)) << item_text
                    end
                    box_width = game_paused_text.global_bounds.width
                    @pause_menu.as(Array(SF::Text)).each do |t|
                        if t.global_bounds.width > box_width
                            box_width = t.global_bounds.width
                        end
                    end
                    box_width = box_width + 24
                    box_height = 32*(2+@pause_menu_items.size)+24
                    pause_menu_background = SF::RectangleShape.new(SF.vector2(box_width, box_height))
                    pause_menu_background.fill_color = @global_config.background_color
                    pause_menu_background.outline_color = @global_config.wall_color
                    pause_menu_background.outline_thickness = 5
                    pause_menu_background.position = SF.vector2(@global_config.render_width/2-box_width/2, @global_config.render_height/2-box_height/2)
                    @current_render.draw(pause_menu_background)
                    game_paused_text.position = SF.vector2(@global_config.render_width/2-game_paused_text.global_bounds.width/2, @global_config.render_height/2-box_height/2+12)
                    @current_render.draw(game_paused_text)
                    pos_iter = 2
                    @pause_menu.as(Array(SF::Text))[@pause_menu_active_item].color = @global_config.selected_text_color
                    @pause_menu.as(Array(SF::Text))[@pause_menu_active_item].style = SF::Text::Bold
                    @pause_menu.as(Array(SF::Text)).each do |t|
                        t.position = SF.vector2(@global_config.render_width/2-t.global_bounds.width/2, @global_config.render_height/2-box_height/2+(32*pos_iter))
                        @current_render.draw(t)
                        pos_iter += 1
                    end
                elsif running?
                    move_snake
                elsif death?
                    @current_render.draw(Snake::GameShapes.pause_overlay)
                    @death_menu = [] of SF::Text
                    snek_ded_text = SF::Text.new
                    snek_ded_text.font = @global_config.default_font
                    snek_ded_text.character_size = 24
                    snek_ded_text.string = "S N E K  D E D"
                    snek_ded_text.color = @global_config.text_color
                    snek_ded_text.style = SF::Text::Bold
                    final_score_label_text = SF::Text.new
                    final_score_label_text.font = @global_config.default_font
                    final_score_label_text.character_size = 24
                    final_score_label_text.string = "FINAL SCORE: "
                    final_score_label_text.color = @global_config.text_color
                    final_score_label_text.style = SF::Text::Bold
                    final_score_text = SF::Text.new
                    final_score_text.font = @global_config.default_font
                    final_score_text.character_size = 24
                    final_score_text.string = @score.to_s
                    final_score_text.color = @global_config.text_color
                    @death_menu_items.each do |i|
                        item_text = SF::Text.new
                        item_text.font = @global_config.default_font
                        item_text.character_size = 24
                        item_text.string = i
                        item_text.color = @global_config.text_color
                        @death_menu.as(Array(SF::Text)) << item_text
                    end
                    box_width = snek_ded_text.global_bounds.width
                    if final_score_label_text.global_bounds.width + final_score_text.global_bounds.width > box_width
                        box_width = final_score_label_text.global_bounds.width + final_score_text.global_bounds.width
                    end
                    @death_menu.as(Array(SF::Text)).each do |t|
                        if t.global_bounds.width > box_width
                            box_width = t.global_bounds.width
                        end
                    end
                    box_width = box_width + 24
                    box_height = 32*(4+@pause_menu_items.size)+24
                    death_menu_background = SF::RectangleShape.new(SF.vector2(box_width, box_height))
                    death_menu_background.fill_color = @global_config.background_color
                    death_menu_background.outline_color = @global_config.wall_color
                    death_menu_background.outline_thickness = 5
                    death_menu_background.position = SF.vector2(@global_config.render_width/2-box_width/2, @global_config.render_height/2-box_height/2)
                    @current_render.draw(death_menu_background)
                    snek_ded_text.position = SF.vector2(@global_config.render_width/2-snek_ded_text.global_bounds.width/2, @global_config.render_height/2-box_height/2+12)
                    final_score_label_text.position = SF.vector2(@global_config.render_width/2-(final_score_label_text.global_bounds.width+final_score_text.global_bounds.width)/2, @global_config.render_height/2-box_height/2+(32*2))
                    final_score_text.position = SF.vector2(final_score_label_text.global_bounds.left+final_score_label_text.global_bounds.width, final_score_label_text.global_bounds.top-final_score_label_text.local_bounds.top)
                    @current_render.draw(snek_ded_text)
                    @current_render.draw(final_score_label_text)
                    @current_render.draw(final_score_text)
                    pos_iter = 4
                    @death_menu.as(Array(SF::Text))[@death_menu_active_item].color = @global_config.selected_text_color
                    @death_menu.as(Array(SF::Text))[@death_menu_active_item].style = SF::Text::Bold
                    @death_menu.as(Array(SF::Text)).each do |t|
                        t.position = SF.vector2(@global_config.render_width/2-t.global_bounds.width/2, @global_config.render_height/2-box_height/2+(32*pos_iter))
                        @current_render.draw(t)
                        pos_iter += 1
                    end
                end
                @current_render.draw(@score_text)
                @current_render.draw(@score_content_text)
            else
                title_text = SF::Text.new
                title_text.font = @global_config.default_font
                title_text.character_size = 192
                title_text.string = "SNEK"
                title_text.color = @global_config.text_color
                title_text.style = SF::Text::Bold
                title_text.position = SF.vector2(@global_config.render_width/2-(title_text.local_bounds.left+title_text.local_bounds.width)/2, @global_config.render_height/8-((title_text.local_bounds.top+title_text.local_bounds.height)/2))
                @current_render.draw(title_text)
                sound_toggle_text = SF::Text.new
                sound_toggle_text.font = @global_config.default_font
                sound_toggle_text.character_size = 24
                sound_toggle_text.string = "[M]"
                sound_toggle_text.color = @global_config.text_color
                if @sounds
                    sound_toggle_text.position = SF.vector2(@sounds_enabled_sprite.global_bounds.left-sound_toggle_text.local_bounds.width-5, @sounds_enabled_sprite.global_bounds.top)
                    @sounds_enabled_sprite.position = SF.vector2(@global_config.render_width-@sounds_enabled_sprite.local_bounds.width-2,0)
                    @current_render.draw(@sounds_enabled_sprite)
                else
                    sound_toggle_text.position = SF.vector2(@sounds_disabled_sprite.global_bounds.left-sound_toggle_text.local_bounds.width-5, @sounds_disabled_sprite.global_bounds.top)
                    @sounds_disabled_sprite.position = SF.vector2(@global_config.render_width-@sounds_disabled_sprite.local_bounds.width-2,0)
                    @current_render.draw(@sounds_disabled_sprite)
                end
                @current_render.draw(sound_toggle_text)
                @main_menu_selected_speed_text.string = @main_menu_selected_speed.to_s
                @main_menu_selected_speed_text.color = @global_config.text_color
                @main_menu_selected_speed_text.position = SF.vector2(@global_config.render_width/4-@main_menu_selected_speed_text.global_bounds.width/2, @global_config.render_height/8*3-((@main_menu_selected_speed_text.local_bounds.height+@main_menu_selected_speed_text.local_bounds.top+@main_menu_speed_label.local_bounds.height+@main_menu_speed_label.local_bounds.top+@main_menu_speed_hint.local_bounds.height+@main_menu_speed_hint.local_bounds.top)/2))
                @current_render.draw(@main_menu_selected_speed_text)
                @main_menu_speed_decrease_arrow = Snake::GameShapes.main_menu_arrow(@main_menu_selected_speed_text.local_bounds.height-@main_menu_selected_speed_text.local_bounds.top).as(SF::ConvexShape)
                @main_menu_speed_decrease_arrow.as(SF::ConvexShape).outline_color = @global_config.text_color
                @main_menu_speed_decrease_arrow.as(SF::ConvexShape).outline_thickness = 1
                @main_menu_speed_decrease_arrow.as(SF::ConvexShape).rotation = 180
                @main_menu_speed_decrease_arrow.as(SF::ConvexShape).position = SF.vector2(@global_config.render_width/4-@main_menu_selected_speed_text.global_bounds.width/2-@main_menu_speed_decrease_arrow.as(SF::ConvexShape).global_bounds.width/2-5, @main_menu_selected_speed_text.global_bounds.top+@main_menu_speed_decrease_arrow.as(SF::ConvexShape).global_bounds.height/2)
                if @main_menu_active_item == 0 && (SF::Keyboard.key_pressed?(SF::Keyboard::Left) || (SF::Mouse.button_pressed?(SF::Mouse::Left) && @main_menu_speed_decrease_arrow.as(SF::ConvexShape).global_bounds.contains?(SF.vector2(SF::Mouse.get_position(@window).x/@scale-@x_added/@scale, SF::Mouse.get_position(@window).y/@scale-@y_added/@scale))))
                    @main_menu_speed_decrease_arrow.as(SF::ConvexShape).fill_color = @global_config.text_color
                else
                    @main_menu_speed_decrease_arrow.as(SF::ConvexShape).fill_color = @global_config.background_color
                end
                @current_render.draw(@main_menu_speed_decrease_arrow.as(SF::ConvexShape))
                @main_menu_speed_increase_arrow = Snake::GameShapes.main_menu_arrow(@main_menu_selected_speed_text.local_bounds.height-@main_menu_selected_speed_text.local_bounds.top).as(SF::ConvexShape)
                @main_menu_speed_increase_arrow.as(SF::ConvexShape).outline_color = @global_config.text_color
                @main_menu_speed_increase_arrow.as(SF::ConvexShape).outline_thickness = 1
                @main_menu_speed_increase_arrow.as(SF::ConvexShape).position = SF.vector2(@global_config.render_width/4+@main_menu_selected_speed_text.global_bounds.width/2+@main_menu_speed_increase_arrow.as(SF::ConvexShape).global_bounds.width/2+5, @main_menu_selected_speed_text.global_bounds.top+@main_menu_speed_increase_arrow.as(SF::ConvexShape).global_bounds.height/2)
                if @main_menu_active_item == 0 && (SF::Keyboard.key_pressed?(SF::Keyboard::Right) || (SF::Mouse.button_pressed?(SF::Mouse::Left) && @main_menu_speed_increase_arrow.as(SF::ConvexShape).global_bounds.contains?(SF.vector2(SF::Mouse.get_position(@window).x/@scale-@x_added/@scale, SF::Mouse.get_position(@window).y/@scale-@y_added/@scale))))
                    @main_menu_speed_increase_arrow.as(SF::ConvexShape).fill_color = @global_config.text_color
                else
                    @main_menu_speed_increase_arrow.as(SF::ConvexShape).fill_color = @global_config.background_color
                end
                @current_render.draw(@main_menu_speed_increase_arrow.as(SF::ConvexShape))
                if @main_menu_active_item == 0
                    @main_menu_speed_label.color = @global_config.selected_text_color
                    @main_menu_speed_label.style = SF::Text::Bold
                else
                    @main_menu_speed_label.color = @global_config.text_color
                    @main_menu_speed_label.style = SF::Text::Regular
                end
                @main_menu_speed_label.position = SF.vector2(@global_config.render_width/4-@main_menu_speed_label.global_bounds.width/2, @main_menu_selected_speed_text.global_bounds.top+@main_menu_selected_speed_text.global_bounds.height+5)
                @current_render.draw(@main_menu_speed_label)
                @main_menu_speed_hint.position = SF.vector2(@global_config.render_width/4-@main_menu_speed_hint.global_bounds.width/2, @main_menu_speed_label.global_bounds.top+@main_menu_speed_label.global_bounds.height+3)
                @current_render.draw(@main_menu_speed_hint)
                @main_menu_selected_level_text.string = @levels[@main_menu_active_level].upcase
                @main_menu_selected_level_text.color = @global_config.text_color
                @main_menu_selected_level_text.position = SF.vector2(@global_config.render_width/4*3-@main_menu_selected_level_text.global_bounds.width/2, @main_menu_selected_speed_text.global_bounds.top)
                @main_menu_level_decrease_arrow = Snake::GameShapes.main_menu_arrow(@main_menu_selected_level_text.local_bounds.height-@main_menu_selected_level_text.local_bounds.top).as(SF::ConvexShape)
                @main_menu_level_decrease_arrow.as(SF::ConvexShape).outline_color = @global_config.text_color
                @main_menu_level_decrease_arrow.as(SF::ConvexShape).outline_thickness = 1
                @main_menu_level_decrease_arrow.as(SF::ConvexShape).rotation = 180
                @main_menu_level_decrease_arrow.as(SF::ConvexShape).position = SF.vector2(@global_config.render_width/4*3-@main_menu_selected_level_text.global_bounds.width/2-@main_menu_level_decrease_arrow.as(SF::ConvexShape).global_bounds.width/2-5, @main_menu_selected_level_text.global_bounds.top+@main_menu_level_decrease_arrow.as(SF::ConvexShape).global_bounds.height/2)
                if @main_menu_active_item == 1 && (SF::Keyboard.key_pressed?(SF::Keyboard::Left) || (SF::Mouse.button_pressed?(SF::Mouse::Left) && @main_menu_level_decrease_arrow.as(SF::ConvexShape).global_bounds.contains?(SF.vector2(SF::Mouse.get_position(@window).x/@scale-@x_added/@scale, SF::Mouse.get_position(@window).y/@scale-@y_added/@scale))))
                    @main_menu_level_decrease_arrow.as(SF::ConvexShape).fill_color = @global_config.text_color
                else
                    @main_menu_level_decrease_arrow.as(SF::ConvexShape).fill_color = @global_config.background_color
                end
                @current_render.draw(@main_menu_level_decrease_arrow.as(SF::ConvexShape))
                @main_menu_level_increase_arrow = Snake::GameShapes.main_menu_arrow(@main_menu_selected_level_text.local_bounds.height-@main_menu_selected_level_text.local_bounds.top).as(SF::ConvexShape)
                @main_menu_level_increase_arrow.as(SF::ConvexShape).outline_color = @global_config.text_color
                @main_menu_level_increase_arrow.as(SF::ConvexShape).outline_thickness = 1
                @main_menu_level_increase_arrow.as(SF::ConvexShape).position = SF.vector2(@global_config.render_width/4*3+@main_menu_selected_level_text.global_bounds.width/2+@main_menu_level_increase_arrow.as(SF::ConvexShape).global_bounds.width/2+5, @main_menu_selected_level_text.global_bounds.top+@main_menu_level_increase_arrow.as(SF::ConvexShape).global_bounds.height/2)
                if @main_menu_active_item == 1 && (SF::Keyboard.key_pressed?(SF::Keyboard::Right) || (SF::Mouse.button_pressed?(SF::Mouse::Left) && @main_menu_level_increase_arrow.as(SF::ConvexShape).global_bounds.contains?(SF.vector2(SF::Mouse.get_position(@window).x/@scale-@x_added/@scale, SF::Mouse.get_position(@window).y/@scale-@y_added/@scale))))
                    @main_menu_level_increase_arrow.as(SF::ConvexShape).fill_color = @global_config.text_color
                else
                    @main_menu_level_increase_arrow.as(SF::ConvexShape).fill_color = @global_config.background_color
                end
                @current_render.draw(@main_menu_level_increase_arrow.as(SF::ConvexShape))
                @current_render.draw(@main_menu_selected_level_text)
                if @main_menu_active_item == 1
                    @main_menu_level_label.color = @global_config.selected_text_color
                    @main_menu_level_label.style = SF::Text::Bold
                else
                    @main_menu_level_label.color = @global_config.text_color
                    @main_menu_level_label.style = SF::Text::Regular
                end
                @main_menu_level_label.position = SF.vector2(@global_config.render_width/4*3-@main_menu_level_label.global_bounds.width/2, @main_menu_speed_label.global_bounds.top)
                @current_render.draw(@main_menu_level_label)
                if @main_menu_active_item == 2
                    @main_menu_start_text.color = @global_config.selected_text_color
                    @main_menu_start_text.style = SF::Text::Bold
                else
                    @main_menu_start_text.color = @global_config.text_color
                    @main_menu_start_text.style = SF::Text::Regular
                end
                @main_menu_start_text.position = SF.vector2(@global_config.render_width/2-(@main_menu_quit_text.local_bounds.left+@main_menu_start_text.local_bounds.width)/2, (@global_config.render_height/2)+5)
                @current_render.draw(@main_menu_start_text)
                if @main_menu_active_item == 3
                    @main_menu_quit_text.color = @global_config.selected_text_color
                    @main_menu_quit_text.style = SF::Text::Bold
                else
                    @main_menu_quit_text.color = @global_config.text_color
                    @main_menu_quit_text.style = SF::Text::Regular
                end
                @main_menu_quit_text.position = SF.vector2(@global_config.render_width/2-(@main_menu_quit_text.local_bounds.left+@main_menu_quit_text.local_bounds.width)/2, (@global_config.render_height/2)+@main_menu_start_text.global_bounds.height+10)
                @current_render.draw(@main_menu_quit_text)
            end
            current_render_view = @current_render.default_view
            current_render_view.reset(SF.float_rect(0, 0, @global_config.render_width, @global_config.render_height))
            @current_render.view = current_render_view
            @current_render.display
            current_render_texture = @current_render.texture
            current_render_texture.smooth = true
            current_render_sprite = SF::Sprite.new(current_render_texture)
            current_render_sprite.scale = SF.vector2(1.0, 1.0)
            current_render_sprite.position = SF.vector2(0.0, 0.0)
            apply_global_scale(current_render_sprite)
            @window.draw(current_render_sprite)
        end

        def paused?
            @state == "paused"
        end

        def running?
            @state == "running"
        end

        def death?
            @state == "death"
        end

        def main?
            @state == "main"
        end

        def pause!
            if @state == "running"
                @next_snake_dir = nil
                @state = "paused"
                @pause_menu_active_item = 0
            end
        end

        def run!
            if @state == "paused" || @state == "death" || @state == "main"
                @state = "running"
                @snake_clock.restart
            end
        end

        def die!
            if @state == "running"
                play_sound("ded")
                @next_snake_dir = nil
                @state = "death"
                @death_menu_active_item = 0
            end
        end

        def main!
            @next_snake_dir = nil
            @state = "main"
        end

        def switch_death_menu_item(item)
            if item != @death_menu_active_item
                @death_menu_active_item = item
                if @sounds
                    play_sound("click")
                end
            end
        end

        def switch_pause_menu_item(item)
            if item != @pause_menu_active_item
                @pause_menu_active_item = item
                if @sounds
                    play_sound("click")
                end
            end
        end

        def switch_main_menu_item(item)
            if item != @main_menu_active_item
                @main_menu_active_item = item
                play_sound("click")
            end
        end

        def process_event(event)
            case event
            when SF::Event::KeyPressed
                case event.code
                when SF::Keyboard::Escape
                    case @state
                    when "paused"
                        play_sound("click")
                        run!
                    when "running"
                        play_sound("click")
                        pause!
                    end
                when SF::Keyboard::Return
                    case @state
                    when "paused"
                        execute_pause_menu_action
                    when "death"
                        execute_death_menu_action
                    when "main"
                        execute_main_menu_action
                    end
                when SF::Keyboard::Up
                    case @state
                    when "paused"
                        return if @pause_menu.nil?
                        next_pause_menu_item = @pause_menu_active_item-1
                        if next_pause_menu_item < 0
                            next_pause_menu_item = @pause_menu_items.size-1
                        end
                        switch_pause_menu_item(next_pause_menu_item)
                    when "running"
                        if (@snake_dir == 'l' || @snake_dir == 'r') && @next_snake_dir.nil?
                            @next_snake_dir = 'u'
                        end
                    when "death"
                        return if @death_menu.nil?
                        next_death_menu_item = @death_menu_active_item-1
                        if next_death_menu_item < 0
                            next_death_menu_item = @death_menu_items.size-1
                        end
                        switch_death_menu_item(next_death_menu_item)
                    when "main"
                        next_main_menu_item = @main_menu_active_item-1
                        if next_main_menu_item < 0
                            next_main_menu_item = 3
                        end
                        switch_main_menu_item(next_main_menu_item)
                    end
                when SF::Keyboard::Down
                    case @state
                    when "paused"
                        return if @pause_menu.nil?
                        next_pause_menu_item = @pause_menu_active_item+1
                        if next_pause_menu_item >= @pause_menu_items.size
                            next_pause_menu_item = 0
                        end
                        switch_pause_menu_item(next_pause_menu_item)
                    when "running"
                        if (@snake_dir == 'l' || @snake_dir == 'r') && @next_snake_dir.nil?
                            @next_snake_dir = 'd'
                        end
                    when "death"
                        return if @death_menu.nil?
                        next_death_menu_item = @death_menu_active_item+1
                        if next_death_menu_item >= @death_menu_items.size
                            next_death_menu_item = 0
                        end
                        switch_death_menu_item(next_death_menu_item)
                    when "main"
                        next_main_menu_item = @main_menu_active_item+1
                        if next_main_menu_item > 3
                            next_main_menu_item = 0
                        end
                        switch_main_menu_item(next_main_menu_item)
                    end
                when SF::Keyboard::Right
                    case @state
                    when "running"
                        if (@snake_dir == 'u' || @snake_dir == 'd') && @next_snake_dir.nil?
                            @next_snake_dir = 'r'
                        end
                    when "main"
                        case @main_menu_active_item
                        when 0
                            @main_menu_selected_speed += 1
                            play_sound("click")
                        when 1
                            @main_menu_active_level += 1
                            if @main_menu_active_level >= @levels.size
                                @main_menu_active_level = 0
                            end
                            play_sound("click")
                        end
                    end
                when SF::Keyboard::Left
                    case @state
                    when "running"
                        if (@snake_dir == 'u' || @snake_dir == 'd') && @next_snake_dir.nil?
                            @next_snake_dir = 'l'
                        end
                    when "main"
                        case @main_menu_active_item
                        when 0
                            @main_menu_selected_speed -= 1
                            if @main_menu_selected_speed < 1
                                @main_menu_selected_speed = 1
                            end
                            play_sound("click")
                        when 1
                            @main_menu_active_level -= 1
                            if @main_menu_active_level < 0
                                @main_menu_active_level = @levels.size-1
                            end
                            play_sound("click")
                        end
                    end
                when SF::Keyboard::M
                    @sounds = !@sounds
                    play_sound("click")
                end
            when SF::Event::MouseMoved
                scaled_x = event.x/@scale-@x_added/@scale
                scaled_y = event.y/@scale-@y_added/@scale
                case @state
                when "paused"
                    return if @pause_menu.nil?
                    next_pause_menu_item = 0
                    @pause_menu.as(Array(SF::Text)).each do |t|
                        if t.global_bounds.contains?(SF.vector2(scaled_x, scaled_y))
                            switch_pause_menu_item(next_pause_menu_item)
                            break
                        end
                        next_pause_menu_item += 1
                    end
                when "death"
                    return if @death_menu.nil?
                    next_death_menu_item = 0
                    @death_menu.as(Array(SF::Text)).each do |t|
                        if t.global_bounds.contains?(SF.vector2(scaled_x, scaled_y))
                            switch_death_menu_item(next_death_menu_item)
                            break
                        end
                        next_death_menu_item += 1
                    end
                when "main"
                    position = SF.vector2(scaled_x, scaled_y)
                    if @main_menu_selected_speed_text.global_bounds.contains?(position) || @main_menu_speed_decrease_arrow.as(SF::ConvexShape).global_bounds.contains?(position) || @main_menu_speed_increase_arrow.as(SF::ConvexShape).global_bounds.contains?(position) || @main_menu_speed_label.global_bounds.contains?(position)
                        switch_main_menu_item(0)
                    elsif @main_menu_selected_level_text.global_bounds.contains?(position) || @main_menu_level_decrease_arrow.as(SF::ConvexShape).global_bounds.contains?(position) || @main_menu_level_increase_arrow.as(SF::ConvexShape).global_bounds.contains?(position) || @main_menu_level_label.global_bounds.contains?(position)
                        switch_main_menu_item(1)
                    elsif @main_menu_start_text.global_bounds.contains?(position)
                        switch_main_menu_item(2)
                    elsif @main_menu_quit_text.global_bounds.contains?(position)
                        switch_main_menu_item(3)
                    end
                end
            when SF::Event::MouseButtonPressed
                scaled_x = event.x/@scale-@x_added/@scale
                scaled_y = event.y/@scale-@y_added/@scale
                if event.button.left?
                    case @state
                    when "paused"
                        return if @pause_menu.nil?
                        next_pause_menu_item = 0
                        @pause_menu.as(Array(SF::Text)).each do |t|
                            if t.global_bounds.contains?(SF.vector2(scaled_x, scaled_y))
                                switch_pause_menu_item(next_pause_menu_item)
                                break
                            end
                            next_pause_menu_item += 1
                        end
                        if next_pause_menu_item == @pause_menu_active_item
                            execute_pause_menu_action
                        end
                    when "death"
                        return if @death_menu.nil?
                        next_death_menu_item = 0
                        @death_menu.as(Array(SF::Text)).each do |t|
                            if t.global_bounds.contains?(SF.vector2(scaled_x, scaled_y))
                                switch_death_menu_item(next_death_menu_item)
                                break
                            end
                            next_death_menu_item += 1
                        end
                        if next_death_menu_item == @death_menu_active_item
                            execute_death_menu_action
                        end
                    when "main"
                        execute_action = true
                        position = SF.vector2(scaled_x, scaled_y)
                        if @main_menu_selected_speed_text.global_bounds.contains?(position) || @main_menu_speed_decrease_arrow.as(SF::ConvexShape).global_bounds.contains?(position) || @main_menu_speed_increase_arrow.as(SF::ConvexShape).global_bounds.contains?(position) || @main_menu_speed_label.global_bounds.contains?(position)
                            switch_main_menu_item(0)
                        elsif @main_menu_selected_level_text.global_bounds.contains?(position) || @main_menu_level_decrease_arrow.as(SF::ConvexShape).global_bounds.contains?(position) || @main_menu_level_increase_arrow.as(SF::ConvexShape).global_bounds.contains?(position) || @main_menu_level_label.global_bounds.contains?(position)
                            switch_main_menu_item(1)
                        elsif @main_menu_start_text.global_bounds.contains?(position)
                            switch_main_menu_item(2)
                        elsif @main_menu_quit_text.global_bounds.contains?(position)
                            switch_main_menu_item(3)
                        elsif @sounds && @sounds_enabled_sprite.global_bounds.contains?(position)
                            @sounds = false
                            execute_action = false
                        elsif !@sounds && @sounds_disabled_sprite.global_bounds.contains?(position)
                            @sounds = true
                            play_sound("click")
                            execute_action = false
                        else
                            execute_action = false
                        end
                        if @main_menu_active_item == 0
                            if @main_menu_speed_decrease_arrow.as(SF::ConvexShape).global_bounds.contains?(position)
                                @main_menu_selected_speed -= 1
                                if @main_menu_selected_speed < 1
                                    @main_menu_selected_speed = 1
                                end
                                play_sound("click")
                                execute_action = false
                            elsif @main_menu_speed_increase_arrow.as(SF::ConvexShape).global_bounds.contains?(position)
                                @main_menu_selected_speed += 1
                                play_sound("click")
                                execute_action = false
                            end
                        elsif @main_menu_active_item == 1
                            if @main_menu_level_decrease_arrow.as(SF::ConvexShape).global_bounds.contains?(position)
                                @main_menu_active_level -= 1
                                if @main_menu_active_level < 0
                                    @main_menu_active_level = @levels.size-1
                                end
                                play_sound("click")
                                execute_action = false
                            elsif @main_menu_level_increase_arrow.as(SF::ConvexShape).global_bounds.contains?(position)
                                @main_menu_active_level += 1
                                if @main_menu_active_level >= @levels.size
                                    @main_menu_active_level = 0
                                end
                                play_sound("click")
                                execute_action = false
                            end
                        end
                        if execute_action
                            execute_main_menu_action
                        end
                    end
                end
            when SF::Event::Resized
                calculate_scaling!(event.width, event.height)
                window_view = @window.default_view
                window_view.reset(SF.float_rect(0,0,event.width, event.height))
                @window.view = window_view
            when SF::Event::LostFocus
                if @state == "running"
                    play_sound("click")
                  pause!
                end
            end
        end

        private def execute_pause_menu_action
            play_sound("click")
            case @pause_menu_items[@pause_menu_active_item]
            when "CONTINUE"
                run!
            when "RESTART"
                spawn_snake(@snake_speed)
                despawn_food
                spawn_food
                despawn_bonus
                @score = 0
                run!
            when "MAIN MENU"
                @score = 0
                main!
            when "QUIT"
                @window.close
            end
        end

        private def execute_death_menu_action
            play_sound("click")
            case @death_menu_items[@death_menu_active_item]
            when "RESTART"
                spawn_snake(@snake_speed)
                despawn_food
                spawn_food
                despawn_bonus
                @score = 0
                run!
            when "MAIN MENU"
                @score = 0
                main!
            when "QUIT"
                @window.close
            end
        end

        private def execute_main_menu_action
            play_sound("click")
            case @main_menu_active_item
            when 2
                load_level("levels/#{@levels[@main_menu_active_level]}.slvl")
                spawn_snake(@main_menu_selected_speed)
                despawn_food
                spawn_food
                despawn_bonus
                @score = 0
                run!
            when 3
                @window.close
            end
        end

        private def calculate_scaling!(width, height)
            ratio = width.to_f32/height.to_f32
            if (ratio - (4.0/3.0)).abs < 0.00001
                @x_added = 0.0.to_f32
                @y_added = 0.0.to_f32
                @scale = (width.to_f32 / @global_config.render_width.to_f32).as(Float32)
            elsif ratio > (4.0/3.0)
                @scale = (height.to_f32 / @global_config.render_height.to_f32).as(Float32)
                @x_added = ((width.to_f32 - (@global_config.render_width.to_f32*@scale))/2).as(Float32)
                @y_added = 0.0.to_f32
            elsif ratio < (4.0/3.0)
                @scale = (width.to_f32 / @global_config.render_width.to_f32).as(Float32)
                @x_added = 0.0.to_f32
                @y_added = ((height.to_f32 - (@global_config.render_height.to_f32*@scale))/2).as(Float32)
            end
        end

        private def apply_global_scale(entity : SF::Transformable)
            entity.scale(SF.vector2(@scale.as(Float32), @scale.as(Float32)))
            entity.move(SF.vector2(@x_added.as(Float32), @y_added.as(Float32)))
            entity
        end

        private def apply_global_scale(entity : Nil)
            entity
        end
    end
end
