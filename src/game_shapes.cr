require "crsfml"
require "./global_config"

module Snake
    module GameShapes
        extend self

        def wall_square(grid_position : Tuple(Int32, Int32))
            shape = SF::RectangleShape.new(SF.vector2(16,16))
            shape.position = SF.vector2(grid_position[0]*16, grid_position[1]*16)
            shape.fill_color = Snake::GlobalConfig.instance.wall_color
            shape
        end

        def snake_part(grid_position : Tuple(Int32, Int32))
            shape = SF::CircleShape.new(7)
            shape.position = SF.vector2(grid_position[0]*16+1, grid_position[1]*16+1)
            shape.fill_color = Snake::GlobalConfig.instance.snake_color
            shape
        end

        def pause_overlay
            shape = SF::RectangleShape.new(SF.vector2(Snake::GlobalConfig.instance.render_width, Snake::GlobalConfig.instance.render_height))
            shape.fill_color = SF::Color.new(255,255,255,100)
            shape
        end

        def main_menu_arrow(a)
            shape = SF::ConvexShape.new
            shape.point_count = 3
            shape[0] = SF.vector2(0,0)
            shape[1] = SF.vector2(0,a)
            shape[2] = SF.vector2(a/2,a/2)
            shape.origin = SF.vector2(0,a/2)
            shape
        end

        def food_square(grid_position : Tuple(Int32, Int32))
            shape = SF::RectangleShape.new(SF.vector2(10, 10))
            shape.position = SF.vector2(grid_position[0]*16+3, grid_position[1]*16+3)
            shape.fill_color = Snake::GlobalConfig.instance.food_color
            shape
        end

        def bonus_triangle(grid_position : Tuple(Int32, Int32))
            shape = SF::ConvexShape.new
            shape.point_count = 3
            shape[0] = SF.vector2(0,10)
            shape[1] = SF.vector2(10,10)
            shape[2] = SF.vector2(5,0)
            shape.position = SF.vector2(grid_position[0]*16+3, grid_position[1]*16+3)
            shape.fill_color = Snake::GlobalConfig.instance.bonus_color
            shape
        end
    end
end