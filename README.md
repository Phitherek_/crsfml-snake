# Snek - a Snake game in CrSFML

## Why?

Why not!

## Prerequisites

* SFML development files >= 2.3
* Crystal >= 0.25
* CrSFML (included as a submodule)

## Build

Build CrSFML with VoidCSFML:
```bash
git submodule init
git submodule update
./build_crsfml.sh
```

Install CrSFML with VoidCSFML:
```bash
./install_crsfml.sh
```

Build the game:
```bash
./build.sh
```

## Run

Run the game:
```bash
./run.sh
```

## Customization

You can tweak the options in `config/global.yml` to change screen size, font or colors, but do so at your own risk.

## Levels

You can add your own levels in `levels` directory. Levels are text files (UTF-8 encoding) with `.slvl` extension. Inside is 64x46 grid (spaces and newlines are ignored) of numbers - `0` is empty space, `1` is wall, `2` is snake head spawn point.

## Contributing

Issues and pull requests are welcome, also for new levels.

## Acknowledgements

The following components are used by and distributed alongside the game:

* CrSFML - distributed as a Git submodule, zlib licensed, details in lib/crsfml/LICENSE after downloading submodule.
* Terminus TTF - distributed in the TTF font file format, licensed with SIL Open Font License version 1.1, used accordingly.
* Sounds from freesound.org:
    * "Click" by lebaston100 licensed CC-BY 3.0, modified, original: https://freesound.org/people/lebaston100/sounds/192272/
    * "Wah wah trumpet failed joke punch line.wav" by Doctor_Jekyll licensed CC-BY 3.0, modified, original: https://freesound.org/people/Doctor_Jekyll/sounds/240195/
    * Other sounds from public domain (CC0)
* Images and icons from public domain (CC0)

### Have fun!
