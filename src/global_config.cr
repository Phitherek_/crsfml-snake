require "yaml"
require "crsfml"

module Snake
    class GlobalConfig
        def self.instance
            @@instance ||= new
        end

        def raw
            @config
        end

        def initial_window_width
            @window_width ||= @config["window_size"][0].as_i.as(Int32)
        end

        def initial_window_height
            @window_height ||= @config["window_size"][1].as_i.as(Int32)
        end

        def video_mode
            @video_mode ||= SF::VideoMode.new(@config["window_size"][0].as_i.as(Int32), @config["window_size"][1].as_i.as(Int32))
        end

        def render_width
            @render_width ||= @config["render_size"][0].as_i.as(Int32)
        end

        def render_height
            @render_height ||= @config["render_size"][1].as_i.as(Int32)
        end

        def default_font
            @default_font ||= to_font(@config["default_font"].as_s).as(SF::Font)
        end

        def background_color
            @background_color ||= to_color(@config["background_color"]).as(SF::Color)
        end

        def text_color
            @text_color ||= to_color(@config["text_color"]).as(SF::Color)
        end

        def selected_text_color
            @selected_text_color ||= to_color(@config["selected_text_color"]).as(SF::Color)
        end

        def inactive_text_color
            @inactive_text_color ||= to_color(@config["inactive_text_color"]).as(SF::Color)
        end

        def wall_color
            @wall_color ||= to_color(@config["wall_color"]).as(SF::Color)
        end

        def snake_color
            @snake_color ||= to_color(@config["snake_color"]).as(SF::Color)
        end

        def food_color
            @food_color ||= to_color(@config["food_color"]).as(SF::Color)
        end

        def bonus_color
            @bonus_color ||= to_color(@config["bonus_color"]).as(SF::Color)
        end

        private def initialize
            @config = {} of YAML::Any => YAML::Any
            File.open("config/global.yml") do |file|
                @config = YAML.parse(file)
            end
        end

        private def to_font(path)
            SF::Font.from_file(path)
        end

        private def to_color(color_hash)
            SF::Color.new(color_hash["r"].as_i, color_hash["g"].as_i, color_hash["b"].as_i, color_hash["a"].as_i)
        end
    end
end