require "crsfml"
require "yaml"
require "./global_config"
require "./game_grid"

global_config = Snake::GlobalConfig.instance

window = SF::RenderWindow.new(global_config.video_mode, "Snek", SF::Style::Default)
window.vertical_sync_enabled = true
grid = Snake::GameGrid.new(window)

while window.open?
    while event = window.poll_event
        case event
        when SF::Event::Closed
            window.close
        else
            grid.process_event(event)
        end
    end
    window.clear(global_config.background_color)
    grid.draw_frame
    window.display
end